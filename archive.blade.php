@extends(theme_view('layout'))

@section('title')
  Archives - {{ site_title() }}
@stop

@section('content')
  <section class="work"> 

    {{-- Archive Heading --}}
    {{-- Notice the triple brackets to escape any xss for tags and search term. --}}
    @if (isset($tag))
      <h2 class="title">Tag - {{{ ucfirst($tag) }}}</h2>
    @elseif ($search)
      <h2 class="title">Results for {{{ $search }}}</h2>
    @else
      <h2 class="title">Archives</h2>
    @endif
    <hr>
    <ul class="archive">
    @foreach ($posts as $post)
        <span><br><i class="icon-calendar">&nbsp;</i> {{ date("M d, Y", strtotime($post['publish_date'])) }}</span> - <strong><a href="{{ wardrobe_url('/post/'.$post['slug']) }}">{{ $post['title'] }}</a></strong>    
    @endforeach
    </ul>
  </section>
@stop
