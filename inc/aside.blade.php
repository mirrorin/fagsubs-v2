<!-- Latest Articles --> 
@include(theme_view('inc.aside.latest_articles'))  

<!-- Links --> 
@include(theme_view('inc.aside.link'))

<!-- Tags --> 
@include(theme_view('inc.aside.tags'))

<!-- Credits --> 
<footer>
    <small class="pull-right">
        Powered by <a href="http://wardrobecms.com" target="_blank"><i class="fa fa-heart"></i></a> and <a href="">Twitter Bootsrap</a>
    </small>     
</footer>
 